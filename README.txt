
Overview
---------

IMAP stands for Internet Message Access Protocol. It is a method of accessing 
electronic mail or bulletin board messages that are kept on a (possibly shared) 
mail server. In other words, it permits a "client" email program to access 
remote message stores as if they were local. For example, email stored on an 
IMAP server can be manipulated from a desktop computer at home, a workstation at 
the office, and a notebook computer while traveling, without the need to 
transfer messages or files back and forth between these computers.

POP3 stands for Post Office Protocol version 3 (POP3), an application-layer 
Internet standard protocol, to retrieve e-mail from a remote server over a 
TCP/IP connection. 

The Network News Transfer Protocol or NNTP is an Internet application protocol 
used primarily for reading and posting Usenet articles, as well as transferring 
news among news servers.

This module allows to your Drupal users to authenticate against one or various 
IMAP/POP3/NNTP servers. They must supply a valid IMAP/POP3/NNTP email/news 
account in the form "user@server" style as username to login.

Requirements
------------
* Drupal 5.x or later
* Php IMAP extension installed on server 
  [http://www.php.net/manual/en/ref.imap.php]

Install
-------

1. Copy the 'imap_auth' module directory in to your Drupal
   modules directory as usual

2. Activate the module via the Drupal module configuration menu
   (Administer >> Site building >> Modules)

3. Configuration: "Administer >> Site configuration >> IMAP auth"

IMAP authentication:
 * Enabled: Users will be able to get logged in using IMAP/POP3/NNTP accounts
 * Disabled : Turn off IMAP authentication.

IMAP Services:
 * Use a single asterisk (*) value to allow IMAP/POP3/NNTP authentication
   against any server
 * Provide a list of valid IMAP/POP3/NNTP servers. Add one server per row with any of these
   formats (you can mix them):
   
  a) "server_alias" 
     Example: mycompany.com
     server_alias is the part after "@" of valid email/news IMAP/POP3/NNTP accounts.
     Module will use "{server_alias:143}INBOX" as default string_mailbox
     to open IMAP connections.
  b) "server_alias,string_mailbox" 
     Example: mycompany.com,{mail.mycompany.com:143/imap/notls}INBOX
     You can set explicit string_mailbox strings plus server_alias. This is useful
     when the part after "@" of valid email IMAP accounts is different
     of IMAP auth server, or when you need to set secure connections or POP3
     connections.
     
     Read more about string_mailbox values:
     http://www.php.net/manual/en/function.imap-open.php

Note:
To allow external user authentication with imap_auth, you need to set 
in "Administer >> User management >> User settings":

  "Public registrations:"
  "Visitors can create accounts and no administrator approval is required."

[http://drupal.org/node/27997]

Authors
-------
Eduin Carrillo   [http://drupal.org/user/16997]
Kenn Persinger   [http://drupal.org/user/25977]

License
-------
GPL (see LICENSE.txt)
